## NAT
[p2p之NAT穿透](https://www.jianshu.com/p/3d64be31857e)

通过双方网络环境不同可分为以下模式：
1. 同一个局域网（无需穿透，直接用ip可以定位）
2. 一个在局域网，一个在外网。
这种情况下，内网主机A访问外网主机B。当主机A访问外网主机B时，通过NAT随机分配一个（外网ip为路由器外网ip）端口，这样就把内网地址映射成了一个唯一的外网地址。然后外网主机B响应主机A时，主机B不直接访问主机A，而是通过NAT转换后的地址访问路由器，路由器就会通过映射把数据分配给内网主机A。这也就是NAT穿透的原理。
3. 都在不同的局域网。
利用一个拥有唯一IP的减重服务器S，两个设备都先向他发送数据包，S可以知道两个设备经过NAT转换后的公网IP，然后需要P2P通信之前，先去服务器查找对方的ip、端口，就可以实现通信。

 **重点**：UDP可以轻松实现穿透。首先创建一个UDP监听端口，然后可以通过这个端口发送和接收数据包。NAT会把这个监听端口映射为外网ip和端口。我们只需要通过端口发送数据包给服务器就可以让服务器拿到这个端口信息，然后可以让其他客户机通过这个端口的信息来发送数据给该端口。
而TCP的话，首先监听一个端口，然后有请求到来时又要为请求分配新的端口，连接关闭后再来新的请求又要分配新的端口，这样一来内网端口映射成外网端口的成本会非常高。几乎不能实现。**非也**，通过复用端口可以实现。
