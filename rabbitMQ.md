## rabbitMQ
消息队列的基本作用：  
分布式环境下提供应用解耦、弹性伸缩、冗余存储、流量削峰、异步通信、数据同步的功能。  
在rabbitMQ中一个消息必须经过exchange操作才能进入队列，不可能直接放入队列中。

<docker run -d -p 15672:15672 -p 5672:5672 --name rabbit rabbitmq>

### 一个简单的通信过程
1. 首先创建连接，需要为连接服务指定执行的主机。
2. 在连接中创建一个频道。
3. 在频道上声明一个队列，需要命名。
4. 然后provider publish，而consumer consume。
5. publish需要指定exchange（交换方式），routing_key（路由键值：用于选择队列）和body（消息体）。
6. consume需要指定callback（回调函数），queue（订阅的队列），no_ack表示不需要接受确认。
7. consume永不退出。

### 工作队列
work queue 用于占用大量资源的任务花费过多的时间等待其完成。我们可以在等待它完成的这段时间可以处理其他事件，所以可以把一个任务封装成一个message并且发送到队列中。  
使用工作队列可以更方便并行工作。扩大规模的时候只需要将更多的consumer用于监听就可以了。  

kill一个worker会丢失处理中和已经分配且未处理的所有消息。  
为了使消息不会丢失，RabbitMQ支持消息确认，consumer会发送确认消息给rabbitMQ告诉rabbitMQ消息已经被确认了，这时rabbitMQ可以释放已经被处理的消息。
没有被ack的消息会被重新入队，会被快速重传到其他consumer手中解决。  
通过ack和prefetch_count可以建造一个工作队列。

### rabbitMQ的数据持久化
通过queue_declare(durable=True)打开数据持久化。  
发布消息时需要将消息的properties设置为delivery_mode=2  

### 公平分派任务
RabbitMQ循环分派任务，保证每个worker收到的任务数量尽可能相等。存在任务工作量不均匀的潜在风险。  
通过设置basic_qoe(prefetch_count=1)可以使得消息队列不会给一个worker分配超过1个任务。

## Exchange模块
exchange有三种模式：direct，topic，fanout。
fanout直接向所有队列进行广播。
direct 直接向指定队列进行转发。
topic 对符合规则的队列进行转发。

## 通过rabbitMQ进行RPC调用
对于每一个RPC请求产生一个回调消息队列时非常低效的，对每一个客户端产生一个则会好很多。
这时我们需要一个correlation_id属性，这个属性用来将response对应到request上。那么会有以下这种情形：消息队列将msg传给client，client接受了，但发送的ack消息队列因为种种原因没收到，于是将统一correlation_id的msg重新传给client，所以我们收到一个用不上的msg，应该忽视并返回ack而不是报错。  
![](https://www.rabbitmq.com/img/tutorials/python-six.png)

客户端启动的时候，创建一个匿名独占的回调消息队列。
对于每一个RPC请求，再头中创建两个字段：reply_to和correlation_id表示返回时接受消息的队列和关联id。

简单RPC服务的服务器逻辑：
0. 声明服务器作为消费者消费的连接、频道、队列（也就是rpc_queue）。（作为生产者的频道由回调函数提供）
1. 作为消费者被call时将参数从消息体中取出执行对应的功能。
2. 在以reply_to作为路由找到对应的消息队列，加入一个带有correlation_id的封装返回结果的消息。
3. 发送完成确认。这里服务器既是生产者又是消费者。

简单RPC服务的客户端逻辑：
0. 声明连接、频道、队列。声明的是接受回调的队列，也就是客户端作为消费者消费的队列。
1. 设置关联id，以关联id和reply_to字段为头，将参数封装到消息中，传入rpc_queue。
2. 一旦回调消息队列中收到消息，则检查关联id是否一致，如果一致，则修改自身的response为对应返回结果。
3. while self.response is None的无限循环被打破，返回函数结果。


rabbitMQ的消费者需要声明，而生产者不需要声明，因为消费者绑定到对应的队列的模式是一定的，而生产者生产的消息由exchange检查后分配到队列中。所以生产者直接生产消息，指定对应的exchange进行任务的分配就好了。  
如果消息处理得太慢可以跑第二个rpc_server.py    
一些简单RPC服务中没有解决的问题：
1. 没有服务器在运行时，客户端怎么反应？应当设置一个超时机制，超过一定时间未响应则视为超时，重新发送请求。
2. 服务器出现故障引发异常，是否需要通知客户端？通过错误码和错误提示加在消息头或者消息体中，返回给客户端。
3. 如何保证无效的（没有经过认证的）请求不会被响应？

### pika 中函数用途
1. BlockingConnection类，初始化时传入主机参数，初始化连接
2. 类中的channel()成员函数，用于创建频道。
3. 大部分函数都由频道提供，queue_declare()创建一个队列。通过exclusive字段设置它只对当前连接有效。
```python
def queue_declare(self, queue='', passive=False, durable=False,exclusive=False, auto_delete=False,arguments=None):
```
4. basic_publish()将消息以指定的routing_key由exchange发送到频道中的队列上。
```python
def basic_publish(self, exchange, routing_key, body,properties=None, mandatory=False, immediate=False):
```
5. basic_consume()收到消息后直接执行callback.
```python
def basic_consume(self,
                      consumer_callback,
                      queue,
                      no_ack=False,
                      exclusive=False,
                      consumer_tag=None,
                      arguments=None):
```

6. basic_qos()提供基础的服务质量保证？引入一个prefetch功能，消费者可以提前将要处理的消息下载下来。通过prefetch_size和count进行限制。只有size小于指定值的消息可以被prefetch，一个consumer最多只能下载不超过指定值的消息。
```python
def basic_qos(self, prefetch_size=0, prefetch_count=0, all_channels=False):
```
7. exchange_declare() 声明一个exchange
```python
def exchange_declare(self,
                         callback=None,
                         exchange=None,
                         exchange_type='direct',
                         passive=False,
                         durable=False,
                         auto_delete=False,
                         internal=False,
                         nowait=False,
                         arguments=None):
```

8. queue_bind()将队列绑定到指定的exchange上，一个队列可以绑定到多个exchange上，也可以多次绑定到同一个exchange上。

9. basic_ack()发送确认消息。
```python
    def basic_ack(self, delivery_tag=0, multiple=False):
        
```

10. basic_get()从队列中取出消息。返回消息的evt.method, evt.properties, evt.body。


### rabbitMQ管理
#### rabbitMQ四种权限
1. management:
用户可以通过AMQP做的任何事外加：
列出自己可以通过AMQP登入的virtual hosts  
查看自己的virtual hosts中的queues, exchanges 和 bindings
查看和关闭自己的channels 和 connections
查看有关自己的virtual hosts的“全局”的统计信息，包含其他用户在这些virtual hosts中的活动。
2. policymaker
management可以做的任何事外加：
查看、创建和删除自己的virtual hosts所属的policies和parameters
3. monitoring
management可以做的任何事外加：
列出所有virtual hosts，包括他们不能登录的virtual hosts
查看其他用户的connections和channels
查看节点级别的数据如clustering和memory使用情况
查看真正的关于所有virtual hosts的全局的统计信息
4. adminstrator
policymaker和monitoring可以做的任何事外加:
创建和删除virtual hosts
查看、创建和删除users
查看创建和删除permissions
关闭其他用户的connections
