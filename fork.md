## 第一章
### 内核
![内核在操作系统中的职能](https://img-blog.csdn.net/20180802160449826?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L09YX2xvdWlz/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)
应用程序通过系统调用在内核空间运行，而内核被称为运行于进程上下文中。这种交互关系——应用程序通过系统调用界面陷入内核——是应用程序完成其工作的基本行为方式。

处理器任何指定时间点上的活动必然概括为下列三者之一：

![这里写图片描述](https://img-blog.csdn.net/20180802161341260?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L09YX2xvdWlz/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)

### 单内核与微内核  
* 单内核：统一存在内核空间中，通信简单，直接函数调用即可。
* 微内核：可以存在用户空间，通过IPC通信，各自独立有效。但是通信需要用户态切换以及IPC机制的代价。
Linux 是单内核实现但是采用微内核设计。支持动态加载内核模块。

## 第二章 内核

## 第三章 进程管理
进程和线程：
![这里写图片描述](https://img-blog.csdn.net/20180802174546315?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L09YX2xvdWlz/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)

任务队列：
存放进程的双向循环链表。

系统调用和异常处理程序是唯二能陷入内核执行的接口。
![这里写图片描述](https://img-blog.csdn.net/20180802201357782?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L09YX2xvdWlz/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)

进程创建
1. fork()通过拷贝当前进程创建一个子进程。
2. exec()读取可执行文件将其载入地址空间开始运行。

#### 深入fork（）

写时拷贝：
是一种可以推迟甚至免除拷贝数据的技术。内核此时不复制整个进程地址空间，而是让父进程和子进程共享同一个拷贝。
在需要写入的时候，拷贝才进行。

fork的实际开销就是复制父进程的页表，以及给子进程创建唯一的进程描述符。

使用fork函数得到的子进程从父进程的继承了整个进程的地址空间，包括：`进程上下文、进程堆栈、内存信息、打开的文件描述符、信号控制设置、进程优先级、进程组号、当前工作目录、根目录、资源限制、控制终端`等。
1. 进程上下文（这个不用多说了，不然接下来子进程怎么执行都不知道）
2. 进程堆栈（共享堆栈中的变量，采取写时拷贝技术，可以极大地减小创建进程的开销）
3. 内存信息（这个暂且死记）
4. 打开的文件描述符（无名管道就是通过文件描述符来定位的，通过父子进程共享文件描述符，无名管道就能在父子进程中通信了）
5. 进程优先级、进程租号、当前工作目录、根目录、资源限制、控制终端
6. 信号量肯定是不能继承的，只是继承信号控制设置。
7. 子进程和父进程拥有独立的地址空间和PID参数。

子进程与父进程的区别在于：

1、父进程设置的锁，子进程不继承（因为如果是排它锁，被继承的话，矛盾了）

2、各自的进程ID和父进程ID不同

3、子进程的未决告警被清除；

4、子进程的未决信号集设置为空集。

![这里写图片描述](https://img-blog.csdn.net/20180802202903516?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L09YX2xvdWlz/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)
1. dup_task_struct()创建新的内核栈，thread_info和task_struct。与当前进程值相同。
2. 检查确保新创建子进程后，进程数目不超过额度。
3. 初始化统计信息。
4. 设置权限。
5. 分配pid
6. 拷贝或共享父进程的资源。
7. 返回子进程的指针。

linux的线程更像轻量级进程。提供线程支持的操作系统通常会有一个指向四个不同线程的进程描述符。而linux仅仅创建4个task_struct结构，指定他们共享某些资源。

进程终结
![这里写图片描述](https://img-blog.csdn.net/20180802205217537?watermark/2/text/aHR0cHM6Ly9ibG9nLmNzZG4ubmV0L09YX2xvdWlz/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70)
1.设置task_struct中的标志。
2.删除定时器？
3.释放空间。计数器--。
4.离开IPC排队的队列。
5.释放资源。
6. 给子进程找养父。设置进程状态。
7. 切换进程。


删除进程描述符不在进程终结同时进行。因为子进程仍有可能需要父进程的信息。
