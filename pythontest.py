#coding=utf-8

# 1
'''
很多人都会误认为list1=[10],list3=['a'],因为他们以为每次extendList被调用时，列表参数的默认值都将被设置为[].但实际上的情况是，新的默认列表只在函数被定义的那一刻创建一次。当extendList被没有指定特定参数list调用时，这组list的值随后将被使用。这是因为带有默认参数的表达式在函数被定义的时候被计算，不是在调用的时候被计算。
因此list1和list3是在同一个默认列表上进行操作（计算）的。而list2是在一个分离的列表上进行操作（计算）的。（通过传递一个自有的空列表作为列表参数的数值）。
'''
def extendList(val, list=[]):
    list.append(val)
    return list

list1 = extendList(10)
list2 = extendList(123,[])
list3 = extendList('a')

print "list1 = %s" % list1
print "list2 = %s" % list2
print "list3 = %s" % list3

# 2

def multipliers():
      return [lambda x : i * x for i in range(4)]

print [m(2) for m in multipliers()]
def multipliers2():
      for i in range(4):
          yield lambda x : i * x 
print [m(2) for m in multipliers2()]
# 可以通过yield关键字声明一个lambda的生成器解决
'''
运行代码，代码从第6行开始运行，解释器碰到了一个列表解析，循环取multipliers()函数中的值，
而multipliers()函数返回的是一个列表对象，这个列表中有4个元素，每个元素都是一个匿名函数
（实际上说是4个匿名函数也不完全准确，其实是4个匿名函数计算后的值，因为后面for i 的循环
不光循环了4次，同时提还提供了i的变量引用，等待4次循环结束后，i指向一个值i=3,这个时候，
匿名函数才开始引用i=3，计算结果。所以就会出现[6,6,6,6]，因为匿名函数中的i并不是立即引用
后面循环中的i值的，而是在运行嵌套函数的时候，才会查找i的值，这个特性也就是延迟绑定
'''


# 3
class Parent(object):
    x = 1

class Child1(Parent):
    pass

class Child2(Parent):
    pass

print Parent.x, Child1.x, Child2.x
Child1.x = 2
print Parent.x, Child1.x, Child2.x
Parent.x = 3
print Parent.x, Child1.x, Child2.x

'''
如果一个变量名没有在当前类下的字典中发现。
则在更高级的类（如它的父类）中进行搜索直到引用的变量名被找到。（如果引用变量名在自身类和更高级类中没有找到，将会引发一个属性错误。）
因此,在父类中设定x = 1,让变量x类(带有值1)能够在其类和其子类中被引用到。这就是为什么第一个打印语句输出结果是1 1 1
因此，如果它的任何一个子类被覆写了值（例如说，当我们执行语句Child.x = 2）,这个值只在子类中进行了修改。这就是为什么第二个打印语句输出结果是1 2 1
最终，如果这个值在父类中进行了修改，（例如说，当我们执行语句Parent.x = 3）,这个改变将会影响那些还没有覆写子类的值（在这个例子中就是Child2）这就是为什么第三打印语句输出结果是3 2 3
'''

# 4
def div1(x,y):
    print "%s/%s = %s" % (x, y, x/y)
    
def div2(x,y):
    print "%s//%s = %s" % (x, y, x//y)

div1(5,2)
div1(5.,2)
div2(5,2)
div2(5.,2.)
