## 数据库相关知识

### 1NF 2NF 3NF 
* 1NF：数据库表的每一列都是不可分割的基本数据项。 
数据项可以分割的数据库风格像mongoDB，文档型数据库。
* 2NF：属性完全依赖于主键。
* 3NF：属性不依赖于其他非主属性。（消除传递依赖）

### MySAM与InnoDB
[MySQL存储引擎——MySAM与InnoDB区别](https://blog.csdn.net/xifeijian/article/details/20316775)  
两种类型最主要的差别就是Innodb 支持事务处理与外键和行级锁。

### MongoDB索引，mysql索引的异同
[聚集索引，非聚集索引](https://www.jianshu.com/p/1775b4ff123a)
都用B+树实现，只是聚集索引存实例，非聚集索引存指针。
聚集索引的辅助索引只能存主键值，而非聚集索引
* 聚集索引：聚集索引表示表中存储的数据按照索引的顺序存储，检索效率比非聚集索引高，存实例。

* 非聚集索引：非聚集索引表示数据存储在一个地方，索引存储在另一个地方，索引带有指针指向数据的存储位置，非聚集索引检索效率比聚集索引低，但对数据更新影响较小。存指针。
![](https://img-blog.csdn.net/20170812110521759?watermark/2/text/aHR0cDovL2Jsb2cuY3Nkbi5uZXQvdTAxMjc1ODA4OA==/font/5a6L5L2T/fontsize/400/fill/I0JBQkFCMA==/dissolve/70/gravity/SouthEast)

### 全文索引
全文索引技术是目前搜索引擎的关键技术。
原理是先定义一个词库，然后在文章中查找每个词条出现的频率和位置，把这样的频率和位置信息按照词库的顺序归纳，这样就相当于对文件建立了一个以词库为目录的索引，这样查找某个词的时候就能很快的定位到改词出现的位置。

### 最左前缀原则
B+树的特性决定的，建立一个（A,B,C）的联合索引，相当于建立了
(A),(A,B),(A,B,C)三个索引。

### sql一次查询最多只能使用一个索引
即使是MongoDB的多键索引，也是先用数组的第一个元素来进行索引，索引完成之后进行筛选。没有更好的方法。Mongo的多键索引感觉有点蠢的，即使建立了多个索引，一次查询就用得到一个，可能也就能在随机单次查询的时候能有点作用了。

### 两段锁协议
可串行性：
并行操作对并行事务的操作的调度是随机的，不同的调度可能产生不同的结果。在这些不同的调度中，肯定有些调度的结果是正确的，究竟哪些调度是正确的呢？  
　　若每个事务的基本操作都串连在一起，没有其它事务的操作与之交叉执行，这样的调度称为串行调度，多个事务的的串行调度，其执行结果一定是正确的。但串行调度限制了系统并行性的发挥，而很多并行调度又不具有串行调度的结果，所以我们必须研究具有串行调度效果的并行调度方法。  
定义：**当且仅当某组并发事务的交叉调度产生的结果和这些事务的某一串行调度的结果相同，则称这个交叉调度是可串行化。**  
两段锁协议规定所有的事务应遵守的规则：
　　1.  在对任何数据进行读、写操作之前，首先要申请并获得对该数据的封锁。
　　2. 在释放一个封锁之后，事务不再申请和获得其它任何封锁。
　　即事务的执行分为两个阶段：
　　第一阶段是获得封锁的阶段，称为扩展阶段。
　　第二阶段是释放封锁的阶段，称为收缩阶段。

### 数据库的ACID特性
原子性、一致性、隔离性和永久性（持续性）。  
**一致性**：一致性可以说是最基本的属性，其他的三个属性都为了保证一致性而存在。分为数据库外部的一致性和数据库内部的一致性。前者由外部应用的编码来完成。后者由数据库本身保证，即在同一个事务内部的一组操作必须全部执行成功（或者全部失败）。这就是事务处理的原子性。  
一致性在传统数据库中还有一种理解就是规则校验，主外键符合规则。
一致性在分布式系统中还分为强一致性和弱一致性（最终一致性）。    
在分布式集群中，最终一致性通常是更合理的选择。

CAP理论：
一致性（C）：在分布式系统中的所有数据备份，在同一时刻是否同样的值。

可用性（A）：在集群中一部分节点故障后，集群整体是否还能响应客户端的读写请求。

分区容忍性（P）：集群中的某些节点在无法联系后，集群整体是否还能继续进行服务。

而CAP理论就是说在分布式存储系统中，最多只能实现上面的两点。而由于当前的网络硬件肯定会出现延迟丢包等问题，所以分区容忍性是我们必须需要实现的。所以我们只能在一致性和可用性之间进行权衡，没有NoSQL系统能同时保证这三点。  

要保证数据强一致性，最简单的方法是令写操作在所有数据节点上都执行成功才能返回成功，也就是同步概念。而这时如果某个结点出现故障，那么写操作就成功不了了，需要一直等到这个节点恢复。也就是说，如果要保证强一致性，那么就无法提供7×24的高可用性。  

而要保证可用性的话，就意味着节点在响应请求时，不用完全考虑整个集群中的数据是否一致。只需要以自己当前的状态进行请求响应。由于并不保证写操作在所有节点都写成功，这可能会导致各个节点的数据状态不一致。  

CAP理论导致了最终一致性和强一致性两种选择。当然，事实上还有其它的选择，比如在Yahoo的PNUTS中，采用的就是松散的一致性和弱可用性结合的方法。但是我们讨论的NoSQL系统没有类似的实现，所以我们在后续不会对其进行讨论。  

**原子性**：为了实现数据库的原子性，需要通过日志：将所有对数据的更新操作都写入日志，如果一个事务中的一部分操作已经成功，但以后的操作，由于断电/系统崩溃/其它的软硬件错误而无法继续，则通过回溯日志，将已经执行成功的操作撤销，从而达到“全部操作失败”的目的。最常见的场景是，数据库系统崩溃后重启，此时数据库处于不一致的状态，必须先执行一个crash recovery的过程：读取日志进行REDO（重演将所有已经执行成功但尚未写入到磁盘的操作，保证持久性），再对所有到崩溃时尚未成功提交的事务进行UNDO（撤销所有执行了一部分但尚未提交的操作，保证原子性）。crash recovery结束后，数据库恢复到一致性状态，可以继续被使用。
**隔离性**：为了保证并发情况下的一致性，引入了隔离性，即保证每一个事务能够看到的数据总是一致的。  
    实现隔离性主要办法是给数据库上锁:悲观锁和乐观锁。
    1. 悲观锁将当前事务所有涉及的操作对象加锁，操作完成后释放给其他对象使用。为了尽可能提高性能，发明了各种粒度（数据库级/表级/行级……）/各种性质（共享锁/排他锁/共享意向锁/排他意向锁/共享排他意向锁……）的锁。为了解决死锁问题，又发明了两阶段锁协议/死锁检测等一系列的技术。
    2. 一种是乐观锁，即不同的事务可以同时看到同一对象（一般是数据行）的不同历史版本。如果有两个事务同时修改了同一数据行，那么在较晚的事务提交时进行冲突检测。实现也有两种，一种是通过日志UNDO的方式来获取数据行的历史版本，一种是简单地在内存中保存同一数据行的多个历史版本，通过时间戳来区分。
    3. 锁的粒度，mysql最小的粒度是行级锁，mongodb最小的粒度是数据库锁，已经比2.2版本之前的全局锁好很多了。
**持久性**：保证已经提交的事务绝对不会消失。

#### MongoDB的伪ACID性质
1. MongoDB的提供对单个文档的原子性，如果想要多个文档的原子性还是不支持的，可以考虑把模型压缩。
2. MongoDB对一致性的支持，是指分布式系统上的支持。分为强一致性和弱一致性两种。
##### 强一致性
强一致性的保证，要求所有数据节点对同一个key值在同一时刻有同样的value值。虽然实际上可能某些节点存储的值是不一样的，但是作为一个整体，当客户端发起对某个key的数据请求时，整个集群对这个key对应的数据会达成一致。下面就举例说明这种一致性是如何实现的。
假设在我们的集群中，一个数据会被备份到N个结点。这N个节点中的某一个可能会扮演协调器的作用。它会保证每一个数据写操作会在成功同步到W个节点后才向客户端返回成功。而当客户端读取数据时，需要至少R个节点返回同样的数据才能返回读操作成功。而NWR之间必须要满足下面关系：R＋W>N  
这样的设计保证了数据未完全同步时读可以检查出错误。
##### 最终一致性
R+W<=N  
#### 隔离性
Mongo3.2之前是采用未提交读的隔离级别。  
Mongo3.2之后采用已提交读的隔离级别。  
#### 持久性
MongoDB使用数据进来先写日志，然后再写入到数据库中的这种方式来保证数据持久性。MongoDB的复制默认节点是三节点以上的复制集群，数据到达主节点后马上回同步到从节点上去。

### 脏读、幻读、不可重复读
[脏读、幻读、不可重复读](http://uule.iteye.com/blog/1109647)
1. 脏读：已修改但是未提交的事务被读取。
2. 不可重复读：在一个事务内多次读同一数据。由于第二个事务的修改，两次读到的数据可能是不一样的。在一个事务内两次读到的数据是不一样的，称为不可重复读。
3. 幻读：是指事务不是独立执行时发生的一种现象。第一个事务对表中全部行进行修改，第二个事务像表中插入一行新数据。就会发生操作第一个事务的用户发现表中还有没有修改的数据行。
不可重复读的重点是修改 :  
 同样的条件, 你读取过的数据,再次读取出来发现值不一样了  
幻读的重点在于新增或者删除  
 同样的条件, 第 1 次和第 2 次读出来的记录数不一样  

#### 四种隔离级别
1. 隔离级别提高会导致并发能力有所下降。
2. 四种标准隔离级别：1.可序列化 2.可重复读3.提交读4.未提交读。

##### 未提交读
是最低的隔离级别。这种级别下，一个事务可以读到另外一个事务未提交的数据。  
锁的情况：事务在读数据的时候并未对数据加锁。修改数据时支队数据增加行级共享锁。  
现象：
1. 事务1、2可以同时读取记录。
2. 一个事务可以读到已修改但未提交的数据。
3. 某行记录同时只能被一个事务写。因为另一个事务不能增加排他写锁进行数据的修改。
4. 这样可以产生脏读。

##### 提交读
在一个事务修改数据过程中，如果事务还没提交，其他事务不能读该数据。  
锁的情况：  
1. 读取数据时加行级共享锁，一旦读完成，立即释放行级共享锁（不会等到事务完成）。
2. 事务在更新数据时加行级排它锁，事务结束时才释放。

现象：  
1. 一旦事务1读取完数据，事务2就可以对数据进行修改。
2. 这样提交读可以解决脏读，但是不能解决不可重复读。

##### 可重复读
锁的情况：  
1. 事务在读取某数据的瞬间（就是开始读取的瞬间），必须先对其加 行级共享锁，直到事务结束才释放；

2. 事务在更新某数据的瞬间（就是发生更新的瞬间），必须先对其加 行级排他锁，直到事务结束才释放。

可以解决脏读和不可重复读。但不能解决幻读，因为加的是行级锁，而非表级锁。在第一次查询后，向表中添加一个符合条件的新的数据项，仍然会导致同一事务中两次查询结果不一致。

##### 可序列化
1. 事务在读取数据时，必须先对其加 表级共享锁 ，直到事务结束才释放；

2. 事务在更新数据时，必须先对其加 表级排他锁 ，直到事务结束才释放。  

虽然可序列化解决了脏读、不可重复读、幻读等读现象。但是序列化事务会产生以下效果：  

1. 无法读取其它事务已修改但未提交的记录。

2. 在当前事务完成之前，其它事务不能修改目前事务已读取的记录。

3. 在当前事务完成之前，其它事务所插入的新记录，其索引键值不能在当前事务的任何语句所读取的索引键范围中。



#### 间隙锁
[MySQL中的锁（表锁、行锁，共享锁，排它锁，间隙锁）](https://blog.csdn.net/soonfly/article/details/70238902)
表级锁：开销小，加锁快；不会出现死锁；锁定粒度大，发生锁冲突的概率最高，并发度最低。   
行级锁：开销大，加锁慢；会出现死锁；锁定粒度最小，发生锁冲突的概率最低，并发度也最高。   
页面锁：开销和加锁时间界于表锁和行锁之间；会出现死锁；锁定粒度界于表锁和行锁之间，并发度一般  
间隙锁：顾名思义，就是对一个查询中符合where的间隙加锁，这样可以防止出现幻读。  
但是存在并发时双间隙锁互相锁定间隙导致死锁的可能性。  

#### MVCC
[浅谈数据库并发控制 - 锁和 MVCC
](https://draveness.me/database-concurrency-control)
到目前为止我们介绍的并发控制机制其实都是通过延迟或者终止相应的事务来解决事务之间的竞争条件（Race condition）来保证事务的可串行化；虽然前面的两种并发控制机制确实能够从根本上解决并发事务的可串行化的问题，但是在实际环境中数据库的事务大都是只读的，读请求是写请求的很多倍，如果写请求和读请求之前没有并发控制机制，那么最坏的情况也是读请求读到了已经写入的数据，这对很多应用完全是可以接受的。
在这种大前提下，数据库系统引入了另一种并发控制机制 - 多版本并发控制（Multiversion Concurrency Control），每一个写操作都会创建一个新版本的数据，读操作会从有限多个版本的数据中挑选一个最合适的结果直接返回；在这时，读写操作之间的冲突就不再需要被关注，而管理和快速挑选数据的版本就成了 MVCC 需要解决的主要问题。

#### MVCC的实现
原来,为了实现mvcc, innodb对每一行都加上了两个隐含的列,其中一列存储行被更新的”时间”,另外一列存储行被删除的”时间”. 但是innodb存储的并不是绝对的时间,而是与时间对应的数据库系统的版本号,每当一个事务开始的时候,innodb都会给这个事务分配一个递增的版本号,所以版本号也可以被认为是事务号.对于每一个”查询”语句,innodb都会把这个查询语句的版本号同这个查询语句遇到的行的版本号进行对比,然后结合不同的事务隔离等级,来决定是否返回该行.  
[深入理解MVCC多版本并发控制](http://blog.51cto.com/donghui/692586)


#### 死锁处理
1. 有向图判环
2. wait-die wound-wait

### 乐观锁与悲观锁
在关系数据库管理系统里，乐观并发控制（又名“乐观锁”，Optimistic Concurrency Control，缩写“OCC”）是一种并发控制的方法。它假设多用户并发的事务在处理时不会彼此互相影响，各事务能够在不产生锁的情况下处理各自影响的那部分数据。在提交数据更新之前，每个事务会先检查在该事务读取数据后，有没有其他事务又修改了该数据。如果其他事务有更新的话，正在提交的事务会进行回滚。

在乐观并发控制中，用户读取数据时不锁定数据。当一个用户更新数据时，系统将进行检查，查看该用户读取数据后其他用户是否又更改了该数据。如果其他用户更新了数据，将产生一个错误。一般情况下，收到错误信息的用户将回滚事务并重新开始。这种方法之所以称为乐观并发控制，是由于它主要在以下环境中使用：数据争用不大且偶尔回滚事务的成本低于读取数据时锁定数据的成本。  

乐观并发控制将事务分为三个阶段：读取阶段、校验阶段以及写入阶段。在读取阶段，事务将数据写入本地缓冲（如上所述，A和B将文件都获取到自己的机器上），此时不会有任何校验操作；在校验阶段，系统会对所有的事务进行同步校验（比如在A或者B打算，但还没有，往SCC上写入更改后的文件时）；在写入阶段，数据将被最终提交。在完成读取阶段以后，系统会对每个事务分派一个时间戳。     
这种策略的缺点是,为了实现多版本,innodb必须对每行增加相应的字段来存储版本信息,同时需要维护每一行的版本信息,而且在检索行的时候,需要进行版本的比较,因而降低了查询的效率;innodb还必须定期清理不再需要的行版本,及时回收空间,这也增加了一些开销
[乐观并发控制与悲观并发控制的区别](https://blog.csdn.net/sanbingyutuoniao123/article/details/79597617)

#### MVCC与乐观锁的区别
多版本并发控制（MVCC）是一种用来解决读-写冲突的无锁并发控制，也就是为事务分配单向增长的时间戳，为每个修改保存一个版本，版本与事务时间戳关联，读操作只读该事务开始前的数据库的快照。 这样在读操作不用阻塞写操作，写操作不用阻塞读操作的同时，避免了脏读和不可重复读  
乐观并发控制（OCC）是一种用来解决写-写冲突的无锁并发控制，认为事务间争用没有那么多，所以先进行修改，在提交事务前，检查一下事务开始后，有没有新提交改变，如果没有就提交，如果有就放弃并重试。乐观并发控制类似自选锁。乐观并发控制适用于低数据争用，写冲突比较少的环境。  
多版本并发控制可以结合基于锁的并发控制来解决写-写冲突，即MVCC+2PL，也可以结合乐观并发控制来解决写-写冲突。  

#### 在SQL中的执行顺序
1. 先连接from后的数据源(若有join，则先执行on后条件，再连接数据源)。 
2. 执行where条件 
3. 执行group by 
4. 执行having 
5. 执行order by 
6. 最后select 输出结果。

#### MySQL中事务的实现
##### 原子性实现：通过回滚实现。
回滚又通过回滚日志实现，所有事务进行的修改都会先记录到回滚日志中，然后再对数据库对应行进行写入。回滚日志不能物理地将数据库恢复到执行语句或事务之前的样子，他是逻辑日志，回滚日志被使用时，他只会逻辑地将数据库中的修改撤销掉看。  
可以理解为，我们在事务中使用的每一条 INSERT 都对应了一条 DELETE，每一条 UPDATE 也都对应一条相反的 UPDATE 语句。  
[数据库中的事务实现](https://draveness.me/mysql-transaction)

##### 持久性实现：通过重做日志实现。
重做日志有两部分：内存中的重做日志缓冲区（易失的），磁盘上的重做日志文件（持久的）。  
![](https://img.draveness.me/2017-08-20-Redo-Logging.jpg-1000width)  
当我们在一个事务中尝试对数据进行修改时，它会先将数据从磁盘读入内存，并更新内存中缓存的数据，然后生成一条重做日志并写入重做日志缓存，当事务真正提交时，MySQL 会将重做日志缓存中的内容刷新到重做日志文件，再将内存中的数据更新到磁盘上，图中的第 4、5 步就是在事务提交时执行的。  
除了所有对数据库的修改会产生重做日志，因为回滚日志也是需要持久存储的，它们也会创建对应的重做日志，在发生错误后，数据库重启时会从重做日志中找出未被更新到数据库磁盘中的日志重新执行以满足事务的持久性。  
回滚日志和重做日志保证两点：
1. 发生错误或者需要回滚的事务能够成功回滚（原子性）；
2. 在事务提交后，数据没来得及写会磁盘就宕机时，在下次重新启动后能够成功恢复数据（持久性）；
隔离性：四种隔离级别或者乐观锁以及多版本并发控制解决的读写冲突，在悲观锁和乐观锁中选择一种解决写-写冲突。
一致性：
ACID中的C包括主键约束、引用约束和约束检查是一致的。还有逻辑上的一致。而CAP的C强调的是分布式的一致性，指的是在多节点上对同一数据的拷贝有相同的值。